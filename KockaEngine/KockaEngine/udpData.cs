﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KockaEngine
{
    public class Player
    {
        public string Name { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Speed { get; set; }
    }

    public class Hero
    {
        public Player Player { get; set; }
        public bool IsReverse { get; set; }
        public int DamageCaused { get; set; }
        public double DistanceCovered { get; set; }
        public int TotalHP { get; set; }
    }

    public class Unit
    {
        public string Filename { get; set; }
        public string Race { get; set; }
        public int Rank { get; set; }
        public string Name { get; set; }
        public int HP { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int DmgMin { get; set; }
        public int DmgMax { get; set; }
        public int Speed { get; set; }
        public int Shoots { get; set; }
        public bool CanShoot { get; set; }
        public int Price { get; set; }
        public int Specials { get; set; }
        public int ShootsAvailable { get; set; }
    }

    public class UDP
    {
        public int ID { get; set; }
        public int Count { get; set; }
        public int CountAtStart { get; set; }
        public int LastHP { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public Hero Hero { get; set; }
        public Unit Unit { get; set; }
        public bool RetaliatedThisRound { get; set; }
        public int TotalHP { get; set; }
        public int Speed { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Threat { get; set; }

        public override bool Equals(object obj)
        {
            return (obj as UDP).ID == this.ID ? true : false;
        }
    }

    public class GameTable
    {
        List<UDP> Enemies;
        List<UDP> Allies;
        public GameTable()
        {
            Enemies = new List<UDP>();
            Allies = new List<UDP>();
        }

        public void AddAlly(UDP uDP)
        {
            if (Allies.Contains(uDP))
            {

            }
        }
    }
}
