﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KockaEngine
{
    public class Shopper
    {
        static public Items Shoping(int money)
        {
            //return new Items() { Names = new string[] { "Paladin", "Peasant", "Peasant", "Peasant", "Peasant" }, Numbers = new int[] {1,1,1,1,1 } };
            if (money < 7500)
            {
                //int maradek = money % 600;
                return new Items() { Names = new string[] { "Halfling", "Halfling", "Goblin", "Goblin", "Goblin" }, Numbers = new int[] { money / 5 /50, money / 5 / 50, money / 5 / 40, money / 5 / 40, money / 5 / 40 } };
            }
            else
            {
                int[] back = new int[] { 1,1,1,1,1 };
                money -= 7500;
                back[0] = (int)(money * 0.15) / 5500;
                back[1] = money / 5 / 600;
                back[2] = money / 5 / 600;
                back[3] = money / 5 / 600;
                back[4] = (int)(money*0.25)/200;

                return new Items() {
                    Names = new string[] { "Titan", "Paladin", "Paladin", "Paladin", "Ranger" },
                    Numbers = back
                };
            }
            
        }
    }

    public class Items
    {
        public string[] Names { get; set; }
        public int[] Numbers { get; set; }

    }
}
