﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.IO;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;
using System.Collections.Generic;

namespace KockaEngine.Network.HTTP
{
    public class Startup
    {
        static Task logger = new Task(() =>{});
        //public static event Func<Context, string> httpRequestEvent;
        public void Configuration(IAppBuilder app)
        {   
            
            //First Layer Logger
            app.Use((context, next) =>
            {
                var ip = context.Request.RemoteIpAddress.ToString();
                //if ( ip== "192.168.1.19" || ip== "192.168.1.244")
                //{
                    return next().ContinueWith(result =>
                    {

                        Parallel.Invoke(() => {
                            TextWriter output = context.Get<TextWriter>("host.TraceOutput");
                            //System.Threading.Thread.Sleep(1000);
                            output.WriteLine("Scheme {0} : Method {1} : Path {2} : MS {3}, {4}",
                            context.Request.Scheme, context.Request.Method, context.Request.Path, getTime(), context.Request.RemoteIpAddress);
                        });

                        /*
                        TextWriter output = context.Get<TextWriter>("host.TraceOutput");
                        output.WriteLine("Scheme {0} : Method {1} : Path {2} : MS {3}",
                        context.Request.Scheme, context.Request.Method, context.Request.Path, getTime());
                        */
                    });
                //}
               // return null;
            });

            

            //2nd Layer API
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/"
                //defaults: new { id = RouteParameter. }
            );
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings =
            new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            
            app.UseWebApi(config);


            //3rd layer catch the rest requests
            app.Run(async context =>
            {

                //var a= httpRequestEvent?.Invoke(new Context() {
                    
                //    yours = context.Request.Query.Get("yours")!=null ? context.Request.Query.Get("yours").Split('|').Select(x => Convert.ToInt32(x)).ToList() : new List<int>(),
                //    thers = context.Request.Query.Get("theirs")!=null ? context.Request.Query.Get("theirs").Split('|').Select(x => Convert.ToInt32(x)).ToList(): new List<int>(),
                //    current =  Convert.ToInt32(context.Request.Query.Get("current")!=null ? context.Request.Query.Get("current") : "-1" ),
                //    money = Convert.ToInt32(context.Request.Query.Get("squadMoney")!=null ? context.Request.Query.Get("squadMoney") : "-1")
                //});
                await context.Response.WriteAsync("a");
            });

            string getTime()
            {
                return DateTime.Now.Millisecond.ToString();
            }
        }
    }
}