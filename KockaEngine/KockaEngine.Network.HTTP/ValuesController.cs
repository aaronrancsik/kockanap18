﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KockaEngine.Network;
using System.Linq;

namespace KockaEngine.Network.HTTP
{
    public class ValuesController : ApiController
    {
        //public static event Func<Context, string> httpRequestEvent;
        // GET api/values 
        public static event Func<Context, Move> requestToMove;
        public static event Func<Context, Items> requestToSopping;

        [HttpGet, Route("{yours=yours}/{theirs=theirs}/{current=current}")]
        public Move Get(string yours,string theirs, string current)
        {
            Console.WriteLine(Request.RequestUri);
            return  requestToMove?.Invoke(new Context()
            {
                yours = yours!=null ? yours.Split('|').Select(x => Convert.ToInt32(x)).ToList() : new List<int>(),
                thers = theirs!=null ? theirs.Split('|').Select(x => Convert.ToInt32(x)).ToList() : new List<int>() ,
                current =  Convert.ToInt32(current) 
            });

            //return httpRequestEvent?.Invoke(new Context() { info1="a",info2="b" });
            //return Mover.Moving(yours.Split('|').Select(x => Convert.ToInt32(x)).ToList(), theirs.Split('|').Select(x => Convert.ToInt32(x)).ToList(), Convert.ToInt32(current));
        }

        [HttpGet, Route("{squadMoney=squadMoney}")]
        public Items Get(string squadMoney)
        {
            return requestToSopping?.Invoke(new Context() { money = Convert.ToInt32(squadMoney) });
        }

        // GET api/values/5 force plaintext
        public HttpResponseMessage Get(int id)
        {
            Console.WriteLine("We got a request");
            string result = "Hello world! Time is: " + DateTime.Now;
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            resp.Content = new StringContent(result, System.Text.Encoding.UTF8, "text/plain");
            return resp;
        }

        // POST api/values 
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5 
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5 
        public void Delete(int id)
        {
        }
    }
}