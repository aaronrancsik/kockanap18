﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KockaEngine.Network.HTTP
{
    public class Server
    {
        static public void Start()
        {
            string baseAddress = "http://192.168.1.244:9000/";
            // Start OWIN host 
            WebApp.Start<Startup>(url: baseAddress);
            
            Console.WriteLine($"OK listening here: {baseAddress}");
        }
    }


}
