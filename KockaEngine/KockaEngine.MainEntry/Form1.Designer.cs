﻿namespace KockaEngine.form
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txUdp = new System.Windows.Forms.TextBox();
            this.txHTTP = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txUdp
            // 
            this.txUdp.Location = new System.Drawing.Point(13, 13);
            this.txUdp.Multiline = true;
            this.txUdp.Name = "txUdp";
            this.txUdp.Size = new System.Drawing.Size(50, 665);
            this.txUdp.TabIndex = 0;
            // 
            // txHTTP
            // 
            this.txHTTP.Location = new System.Drawing.Point(1024, 12);
            this.txHTTP.Multiline = true;
            this.txHTTP.Name = "txHTTP";
            this.txHTTP.Size = new System.Drawing.Size(58, 665);
            this.txHTTP.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(69, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(949, 439);
            this.dataGridView1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 690);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txHTTP);
            this.Controls.Add(this.txUdp);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txUdp;
        public System.Windows.Forms.TextBox txHTTP;
        public System.Windows.Forms.DataGridView dataGridView1;
    }
}

