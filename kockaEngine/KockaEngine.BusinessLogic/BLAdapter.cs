﻿using KockaEngine.Network.Adapter;
using KockaEngine.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using KockaEngine;


namespace KockaEngine.BusinessLogic
{
    public class BLAdapter
    {
        static ApiAdapter api;
        static event Action<Dictionary<int, UDP>> txudp;
        static event Action<Dictionary<int, UDP>> txhttp;
        public BLAdapter(Action<Dictionary<int, UDP>> udp, Action<Dictionary<int, UDP>> http)
        {
            txhttp += http;
            txudp += udp;
            api = new ApiAdapter(respToMove, respToShop, UpdateGameDatas);
            api.StartNetwork();
        }

        static Dictionary<int, UDP> states = new Dictionary<int, UDP>();
        static void UpdateGameDatas(string content, string sender)
        {
            UDP a = JsonConvert.DeserializeObject<UDP>(content);
            try
            {
                states.Add(a.ID, a);
            }
            catch (Exception)
            {

                states[a.ID] = a;
            }
            //txudp?.Invoke(content);


            //dynamic json = JsonConvert.DeserializeObject(content);
            //Console.WriteLine("id: "+json["ID"]+"count: "+json["CountAtStart"]);
            //Console.WriteLine($"BL: {sender} is send this: {content}");
        }

        static Move maxSpeed(List<UDP> enmies, KeyValuePair<int,UDP> cur , double[] vec, double x, double y)
        {
            if (Math.Sqrt(Math.Pow(vec[0], 2) + Math.Pow(vec[1], 2)) > cur.Value.Speed)
            {
                var scalar = cur.Value.Speed / Math.Sqrt(Math.Pow(vec[0], 2) + Math.Pow(vec[1], 2));
                x *= scalar;
                y *= scalar;
                return new Move()
                {
                    MoveX = (cur.Value.X + x).ToString(),
                    MoveY = (cur.Value.Y + y).ToString(),
                    AttackThis = enmies.FirstOrDefault(k => k.Unit.CanShoot) != null ?
                    enmies.FirstOrDefault(k => k.Unit.CanShoot).ID.ToString() :
                    enmies.OrderBy(k => k.TotalHP).First().ID.ToString()
                };
            }

            return new Move() {
                MoveX = vec[0].ToString(),
                MoveY = vec[1].ToString(),
                AttackThis = enmies.FirstOrDefault(k => k.Unit.CanShoot) != null ?
                enmies.FirstOrDefault(k => k.Unit.CanShoot).ID.ToString() :
                enmies.OrderBy(k => k.TotalHP).First().ID.ToString()
            };

        }
        static Move respToMove(Context context)
        {
            txhttp?.Invoke(states);
            var enmies = new List<UDP>();
            foreach (var item in context.thers)
            {
                enmies.Add(states[item]);
            }

            var alies = new List<UDP>();
            foreach (var item in context.yours)
            {
                alies.Add(states[item]);
            }

            //Move move = Mover.Moving(context.yours, context.thers, context.current);
            Console.WriteLine($"BL current: {context.current}");
            var cur = states.Where(k => k.Key == context.current).Single();
            double x = 0;
            double y = 0;
            if (cur.Value.Unit.CanShoot && cur.Value.Unit.Filename!= "Wizard6b_Titan.png")
            {
                 x = 0;
                 y = 0;
                foreach (var item in alies)
                {
                    
                    if (item.ID != cur.Value.ID )
                    {
                        x += item.X;
                        y += item.Y;
                    }
                }
                x /= alies.Count-1;
                y /= alies.Count-1;

                var vec = new double[] { x - cur.Value.X, y - cur.Value.Y };

                return maxSpeed(enmies,cur,vec,x,y);
            }
            else if(cur.Value.Unit.Filename == "Wizard6b_Titan.png")
            {

                 x = alies.FirstOrDefault(k => k.Unit.CanShoot).X;
                 y = alies.FirstOrDefault(k => k.Unit.CanShoot).Y;
                if(x == 0 || y == 0)
                {
                    x = enmies.FirstOrDefault(k => k.Unit.CanShoot).X;
                    y = enmies.FirstOrDefault(k => k.Unit.CanShoot).Y;
                }
                
                if(x == 0 || y == 0)
                {
                    x = enmies.First().X;
                    y = enmies.First().Y;
                }
                var vec = new double[] { x - cur.Value.X, y - cur.Value.Y };
                return maxSpeed(enmies, cur, vec, x, y);
            }

            double minDis = Math.Sqrt( Math.Pow(enmies[0].X, 2) + Math.Pow(enmies[0].Y, 2));
            int minID = 0;
             x = 0;
             y = 0;
            foreach (var item in enmies)
            {
                if (minDis > Math.Sqrt(Math.Pow(item.X, 2) + Math.Pow(item.Y, 2)))
                {
                    minID = item.ID;
                    x = cur.Value.X - item.X;
                    y= cur.Value.Y - item.Y;
                }
                    

                if (  Math.Abs(cur.Value.X - item.X)<=1 || Math.Abs(cur.Value.Y - item.Y) <= 1)
                {
                    
                    return new Move() { MoveX = item.X.ToString(), MoveY = item.Y.ToString(), AttackThis = item.ID.ToString() };
                }

            }

            //states.Where(x => x.Value.ID == minID).Single().Value.X
            var a = new Move() { MoveX = (cur.Value.X + x).ToString(), MoveY=(cur.Value.Y+y).ToString(),  AttackThis = "0"};
            return a;
            /*
            Console.WriteLine("x:" +cur.Value.X+" y: "+cur.Value.Y);
            Console.WriteLine(cur.Value.Hero.IsReverse);
            var a =new Move() {  MoveX = (Convert.ToInt32(cur.Value.X)+1).ToString(),MoveY = cur.Value.Y.ToString(),AttackThis=enmies[0].ID.ToString() };
            Console.WriteLine(a.MoveX+" "+a.MoveY);
            return a;*/
        }

        static void CalcPrio(ref List<UDP> enem, List<UDP> alies)
        {
            foreach (var item in enem)
            {
                item.Threat = 0;
            }
        }

        static Items respToShop(Context context)
        {
            txhttp?.Invoke(states);
            Items items = Shopper.Shoping(context.money);
            Console.WriteLine($"BL money: {context.money}");
            return items;
        }
    }
}
