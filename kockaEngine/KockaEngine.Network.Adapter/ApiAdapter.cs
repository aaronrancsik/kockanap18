﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KockaEngine.Network.HTTP;
using KockaEngine.Network.UDP;
using KockaEngine.Network;

namespace KockaEngine.Network.Adapter
{
    public class ApiAdapter
    {
        public ApiAdapter(Func<Context,Move> reqToMove, Func<Context,Items> reqToShop ,Action<string,string> udpUpdateHandler)
        {
            ValuesController.requestToMove += reqToMove;
            Listener.reciveBroadcast += udpUpdateHandler;
            ValuesController.requestToSopping += reqToShop;
        }

        public void StartNetwork()
        {
            Task server = new Task(Server.Start);
            Task listener = new Task(Listener.StartUDPListener);
            server.Start();
            listener.Start();
        }
    }
}
